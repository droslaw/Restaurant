<?php

use Restaurant\Storage\CourseField as Field;

$offerPath = __DIR__ . '/offer.csv';
ini_set('auto_detect_line_endings', true);
$fields = [Field::CUISINE, Field::TYPE, Field::NAME, Field::PRICE];

$offer = [];

$fileRes = fopen ($offerPath, 'r');
if (false == $fileRes) {
    throw new Exception('Can not load offer.');
}
while (($csvRecord = fgetcsv($fileRes)) !== false) {
    $record = [];
    foreach ($fields as $index => $eachField) {
        $record[$eachField] = $csvRecord[$index];
    }
    $offer[] = $record;
}
fclose ($fileRes);
return $offer;