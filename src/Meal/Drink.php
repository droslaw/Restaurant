<?php

namespace Restaurant\Meal;

class Drink extends MealPart
{
    /**
     * 
     * @var boolean
     */
    private $hasCubes = false;

    /**
     *
     * @var boolean
     */
    private $hasLemon = false;

    public function hasCubes()
    {
        return $this->hasCubes;
    }

    public function hasLemon()
    {
        return $this->hasLemon;
    }

    public function attachCubes($attachCubes = true)
    {
        $this->hasCubes = (boolean) $attachCubes;
    }

    public function attachLemon($attachLemon = true)
    {
        $this->hasLemon = (boolean) $attachLemon;
    }
}