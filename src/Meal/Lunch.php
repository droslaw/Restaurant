<?php

namespace Restaurant\Meal;

use Restaurant\Order\ItemInterface;

class Lunch implements ItemInterface
{

    private $dessert = null;

    private $mainCourse = null;

    public function __construct(MainCourse $mainCourse, Dessert $dessert)
    {
        $this->mainCourse = $mainCourse;
        $this->dessert = $dessert;
    }

    /**
     *
     * @return Course
     */
    public function getDessert()
    {
        return $this->dessert;
    }

    /**
     *
     * @return Course
     */
    public function getMainCourse()
    {
        return $this->mainCourse;
    }

    public function getPrice()
    {
        return $this->dessert->getPrice() + $this->mainCourse->getPrice();
    }
}