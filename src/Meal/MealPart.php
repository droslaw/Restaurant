<?php

namespace Restaurant\Meal;

use Restaurant\Storage\CourseField as Field;
use Restaurant\Order\ItemInterface;

abstract class MealPart implements ItemInterface
{
    const TYPE_DRINK = 'drink';
    const TYPE_DESSERT = 'dessert';
    const TYPE_MAIN_COURSE= 'main-course';

    private $price;
    private $name;
    private $cuisine;

    public function __construct($name, $price, $cuisine=null)
    {
        $this->name = $name;
        $this->price = $price;
        $this->cuisine = $cuisine;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public static function create($data)
    {
        return new static($data[Field::NAME], $data[Field::PRICE]);
    }
}
