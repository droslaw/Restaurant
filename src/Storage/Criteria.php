<?php

namespace Restaurant\Storage;

use Closure;

class Criteria
{
    private $criteriaFunction;

    public function __construct(Closure $criteriaFunction)
    {
        $this->criteriaFunction = $criteriaFunction;
    }

    public function match(array $data)
    {
        $critFnc = $this->criteriaFunction;
        return $critFnc($data);
    }

    public static function equal($fieldName, $expectedValue)
    {
        $fn = function (array $actualValue) use ($fieldName, $expectedValue) {
            return $actualValue[$fieldName] === $expectedValue;
        };
        return new self($fn);
    }
    
    public static function multi(array $criteriaList)
    {
        foreach ($criteriaList as $criteria) {
            if (false === $criteria instanceof Criteria) {
                throw new Exception('Only criteria objects are allowed');
            }
        }
        $fn = function (array $actualValue) use ($criteriaList) {
            foreach ($criteriaList as $criteria) {
                if (false === $criteria->match($actualValue)) {
                    return false;
                }
            }
            return true;
        };
        return new self($fn);
    }
}