<?php
namespace Restaurant\Storage;

use Exception;
use Restaurant\Meal\MealPart;
use Restaurant\Meal\Dessert;
use Restaurant\Meal\MainCourse;
use Restaurant\Meal\Drink;
use Restaurant\Cuisine;

class Repository
{

    private $offer;

    private $cuisines;

    public function __construct(array $offer, array $cuisines)
    {
        $this->offer = $offer;
        foreach ($cuisines as $eachCuisine) {
            $this->cuisines[$eachCuisine->getId()] = $eachCuisine;
        }
    }

    public function getCuisines()
    {
        return array_values($this->cuisines);
    }
    
    /**
     *
     * @param string $id            
     * @return \Restaurant\Meal\Drink
     */
    public function getDrinkById($id)
    {
        return $this->getObjectByIdAndType($id, MealPart::TYPE_DRINK);
    }

    /**
     *
     * @param string $id
     * @return \Restaurant\Meal\Dessert
     */
    public function getDessertById($id)
    {
        return $this->getObjectByIdAndType($id, MealPart::TYPE_DESSERT);
    }

    /**
     *
     * @param string $id
     * @return \Restaurant\Meal\MainCourse
     */
    public function getMainCourseById($id)
    {
        return $this->getObjectByIdAndType($id, MealPart::TYPE_MAIN_COURSE);
    }

    /**
     *
     * @param string $cuisine
     * @return \Restaurant\Meal\Drink[]
     */
    public function listDrinksByCuisine($cuisine)
    {
        return $this->listByCuisineAndType($cuisine, MealPart::TYPE_DRINK);
    }

    /**
     *
     * @param string $cuisine
     * @return \Restaurant\Meal\MainCourse[]
     */
    public function listMainCoursesByCuisine($cuisine)
    {
        return $this->listByCuisineAndType($cuisine, MealPart::TYPE_MAIN_COURSE);
    }

    /**
     *
     * @param string $cuisine
     * @return \Restaurant\Meal\Dessert[]
     */
    public function listDessertsByCuisine($cuisine)
    {
        return $this->listByCuisineAndType($cuisine, MealPart::TYPE_DESSERT);
    }

    private function listByCuisineAndType($cuisine, $type)
    {
        $cuisine = $this->getCuisineId($cuisine);
        $criteria = Criteria::multi([
            Criteria::equal(CourseField::CUISINE, $cuisine),
            Criteria::equal(CourseField::TYPE, $type)
        ]);
        return $this->listObjects($criteria);
    }

    private function listObjects(Criteria $criteria)
    {
        return $this->hydrate($this->filter($criteria));
    }

    private function getObjectByIdAndType($id, $type)
    {
        $criteria = Criteria::multi([
            Criteria::equal(CourseField::NAME, $id),
            Criteria::equal(CourseField::TYPE, $type)
        ]);
        return $this->getObjectOne($criteria);
    }

    private function getObjectOne(Criteria $criteria)
    {
        return $this->createObject($this->find($criteria));
    }

    private function find(Criteria $criteria)
    {
        foreach ($this->offer as $record) {
            if ($criteria->match($record)) {
                return $record;
            }
        }
        throw new Exception('Course not found');
    }

    private function filter(Criteria $criteria)
    {
        $result = [];
        foreach ($this->offer as $mealPart) {
            if ($criteria->match($mealPart)) {
                $result[] = $mealPart;
            }
        }
        return $result;
    }

    private function hydrate(array $data)
    {
        $result = [];
        foreach ($data as $record) {
            $result[] = $this->createObject($record);
        }
        return $result;
    }

    private function createObject(array $record)
    {
        $type = $record[CourseField::TYPE];
        $name = $record[CourseField::NAME];
        $price = $record[CourseField::PRICE];
        $cuisineId = $record[CourseField::CUISINE];
        $cuisine = $this->cuisines[$cuisineId];
        switch ($type) {
            case MealPart::TYPE_DESSERT:
                return new Dessert($name, $price, $cuisine);
            case MealPart::TYPE_DRINK:
                return new Drink($name, $price, $cuisine);
            case MealPart::TYPE_MAIN_COURSE:
                return new MainCourse($name, $price, $cuisine);
        }
        throw new Exception('Invalid meal part type ' . $type);
    }
    
    private function getCuisineId($cuisine)
    {
        if ($cuisine instanceof Cuisine) {
            return $cuisine->getId();
        }
        return $cuisine;
    }
}