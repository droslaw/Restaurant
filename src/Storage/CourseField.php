<?php

namespace Restaurant\Storage;

interface CourseField
{
    const NAME = 'name';
    const PRICE = 'price';
    const TYPE = 'type';
    const CUISINE = 'cuisine';
}
