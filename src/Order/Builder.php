<?php

namespace Restaurant\Order;

use Restaurant\Storage\Repository;
use Restaurant\Order\Order;
use Restaurant\Meal\Lunch;

class Builder
{

    private $repository;

    private $order;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function startOrder()
    {
        $this->order = new Order();
        return $this;
    }

    public function addDrink($id, $includeLemon = false, $includeIceCubes = false)
    {
        $drink = $this->repository->getDrinkById($id);
        $drink->attachLemon((boolean) $includeLemon);
        $drink->attachCubes((boolean) $includeIceCubes);
        
        $this->order->addItem($drink);
        return $this;
    }

    public function addLunch($mainCourseId, $dessertId)
    {
        $mainCourse = $this->repository->getMainCourseById($mainCourseId);
        $dessert = $this->repository->getDessertById($dessertId);
        $lunch = new Lunch($mainCourse, $dessert);
        
        $this->order->addItem($lunch);
        return $this;
    }

    public function getOrder()
    {
        $order = $this->order;
        $this->order = null;
        return $order;
    }
}