<?php
namespace Restaurant\Order;

class Order implements PayableInterface
{

    /**
     *
     * @var \Restaurant\Order\ItemInterface
     */
    private $items = [];

    public function getPrice()
    {
        return array_reduce($this->items, function ($fullPrice, $item) {
            $fullPrice += $item->getPrice();
            return $fullPrice;
        });
    }

    public function getItems()
    {
        return $this->items;
    }

    /**
     *
     * @param ItemInterface $item            
     */
    public function addItem(ItemInterface $item)
    {
        $this->items[] = $item;
    }
}