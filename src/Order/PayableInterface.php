<?php

namespace Restaurant\Order;

interface PayableInterface
{
    public function getPrice();
}