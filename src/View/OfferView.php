<?php

namespace Restaurant\View;

use Restaurant\Storage\Repository;
use Restaurant\Cuisine;
use Restaurant\Meal\MealPart;

class OfferView
{

    private $repository;

    private $mealPartRenderer;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function render()
    {
        $parts = [];
        $cuisines = $this->repository->getCuisines();
        foreach ($cuisines as $eachCuisine) {
            $parts[] = $this->renderCuisineOffer($eachCuisine);
        }
        return implode(PHP_EOL, $parts);
    }

    private function renderCuisineOffer(Cuisine $cuisine)
    {
        $result = 'CUISINE ' . $cuisine->getName() . PHP_EOL;
        
        $result .= '* Drinks:' . PHP_EOL;
        $drinks = $this->repository->listDrinksByCuisine($cuisine);
        $drinkParts = [];
        foreach ($drinks as $eachDrink) {
            $drinkParts[] = '  - ' . $this->renderMealPart($eachDrink);
        }
        $result .= implode(PHP_EOL, $drinkParts) . PHP_EOL;
        
        $result .= '* Desserts:' . PHP_EOL;
        $desserts = $this->repository->listDessertsByCuisine($cuisine);
        $dessertParts = [];
        foreach ($desserts as $eachDessert) {
            $dessertParts[] = '  - ' . $this->renderMealPart($eachDessert);
        }
        $result .= implode(PHP_EOL, $dessertParts) . PHP_EOL;
        
        $result .= '* MainCourses:' . PHP_EOL;
        $mainCourses = $this->repository->listMainCoursesByCuisine($cuisine);
        $mainCoursesParts = [];
        foreach ($mainCourses as $eachMainCourses) {
            $mainCoursesParts[] = '  - ' . $this->renderMealPart($eachMainCourses);
        }
        $result .= implode(PHP_EOL, $mainCoursesParts);
        return $result;
    }
    
    public function renderMealPart(MealPart $mealPart)
    {
        return $mealPart->getName() . ' $' . $mealPart->getPrice();
    }
}