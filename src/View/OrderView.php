<?php
namespace Restaurant\View;

use Restaurant\Order\ItemInterface;
use Restaurant\Meal\Lunch;
use Restaurant\Meal\Drink;
use Restaurant\Order\Order;

class OrderView
{

    public function render(Order $order)
    {
        $result = '';
        foreach ($order->getItems() as $item) {
            $result .= '* ' . $this->renderItem($item) . PHP_EOL;
        }
        $result .= 'Sum $' . $order->getPrice();
        return $result;
    }

    public function renderItem(ItemInterface $item)
    {
        if ($item instanceof Drink) {
            return $this->renderDrink($item);
        } else 
            if ($item instanceof Lunch) {
                return $this->renderLunch($item);
            } else {
                throw new Exception('Can not render item of class ' . get_class($item));
            }
    }

    private function renderDrink(Drink $drink)
    {
        $result = $drink->getName();
        if ($drink->hasLemon()) {
            $result .= ' + Lemon';
        }
        if ($drink->hasCubes()) {
            $result .= ' + Ice cubes';
        }
        $result .= ' $' . $drink->getPrice();
        return $result;
    }

    private function renderLunch(Lunch $lunch)
    {
        $mainCourse = $lunch->getMainCourse();
        $dessert = $lunch->getDessert();
        
        return 'Lunch ' . $this->renderMealPart($mainCourse) . ' + ' . $this->renderMealPart($dessert) . ' $' . $lunch->getPrice();
    }

    public function renderMealPart($mealPart)
    {
        return $mealPart->getName();
    }
}