<?php

$srcDirPath = realpath(__DIR__ . '/src') . '/';

spl_autoload_register(function ($className) use ($srcDirPath) {
    if ('Restaurant\\' !== substr($className, 0, 11)) {
        return false;
    }
    $relClassName = substr($className, 11);
    $relPath = str_replace('\\', '/', $relClassName);
    $classFilePath = $srcDirPath . $relPath . '.php';
    if (file_exists($classFilePath)) {
        require $classFilePath;
        return true;
    }
    return false;
});
