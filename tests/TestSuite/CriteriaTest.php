<?php

use Restaurant\Storage\Criteria;
use Restaurant\Meal\MealPart;
use Restaurant\Storage\CourseField as Field;

class CriteriaTest extends PHPUnit_Framework_TestCase
{
    public function testCriteria()
    {
        $drinkData = [Field::NAME => 'drink1', Field::PRICE => 10,
            Field::TYPE => MealPart::TYPE_DRINK];
        $dessertData = [Field::NAME => 'dessert1', Field::PRICE => 10,
            Field::TYPE => MealPart::TYPE_DESSERT];

        $criteria = Criteria::equal(Field::TYPE, MealPart::TYPE_DESSERT);
        $this->assertTrue($criteria->match($dessertData));
        $this->assertFalse($criteria->match($drinkData));
    }

    public function testMultiCriteria()
    {
        $dessertWithPrice20 = [Field::NAME => 'dessert2', Field::PRICE => 20,
            Field::TYPE => MealPart::TYPE_DESSERT];
        $dessertWithPrice10 = [Field::NAME => 'dessert1', Field::PRICE => 10,
            Field::TYPE => MealPart::TYPE_DESSERT];
    
        $criteria = Criteria::multi([
            Criteria::equal(Field::TYPE, MealPart::TYPE_DESSERT),
            Criteria::equal(Field::PRICE, 20)
        ]);
        $this->assertTrue($criteria->match($dessertWithPrice20));
        $this->assertFalse($criteria->match($dessertWithPrice10));
    }
}