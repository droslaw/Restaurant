<?php

use Restaurant\Meal\Drink;

class DrinkTest extends PHPUnit_Framework_TestCase
{
    public function testDrinkGetPrice()
    {
        $expectedPrice = 100;
        $drink = new Drink('dummy-name', $expectedPrice);
        $this->assertEquals($expectedPrice, $drink->getPrice());
    }
    
    public function testDrinkGetName()
    {
        $expectedName = 'drink-name';
        $drink = new Drink($expectedName, 0);
        $this->assertEquals($expectedName, $drink->getName());
    }

    public function testDrinkDoesNotContainLemonImplicitly()
    {
        $drink = new Drink('dummy-name', 0);
        $this->assertFalse($drink->hasLemon());
    }

    public function testDrinkDoesNotContainCubesImplicitly()
    {
        $drink = new Drink('dummy-name', 0);
        $this->assertFalse($drink->hasCubes());
    }

    public function testCustomerCanAskForLemon()
    {
        $drink = new Drink('dummy-name', 0);
        $drink->attachLemon();
        $this->assertTrue($drink->hasLemon());
    }

    public function testCustomerCanAskForCubes()
    {
        $drink = new Drink('dummy-name', 0);
        $drink->attachCubes();
        $this->assertTrue($drink->hasCubes());
    }
}