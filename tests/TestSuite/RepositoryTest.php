<?php
use Restaurant\Meal\MealPart;
use Restaurant\Storage\CourseField as Field;
use Restaurant\Meal\Drink;
use Restaurant\Meal\Dessert;
use Restaurant\Meal\MainCourse;
use Restaurant\Storage\Repository;
use Restaurant\Cuisine;

const NAME = Field::NAME;

const TYPE = Field::TYPE;

const CUISINE = Field::CUISINE;

const PRICE = Field::PRICE;

const TYPE_DRINK = MealPart::TYPE_DRINK;

const TYPE_MAIN_COURSE = MealPart::TYPE_MAIN_COURSE;

const TYPE_DESSERT = MealPart::TYPE_DESSERT;

class RepositoryTest extends PHPUnit_Framework_TestCase
{

    public function testRepository()
    {
        $polishDrinks = [
            [
                TYPE => TYPE_DRINK,
                CUISINE => 'pl',
                NAME => 'plDrink',
                PRICE => 10
            ]
        ];
        $polishDesserts = [
            [
                TYPE => TYPE_DESSERT,
                CUISINE => 'pl',
                NAME => 'plDessert',
                PRICE => 10
            ]
        ];
        $polishMainCourses = [
            [
                TYPE => TYPE_MAIN_COURSE,
                CUISINE => 'pl',
                NAME => 'plMainCourse',
                PRICE => 10
            ]
        ];
        $italianDrinks = [
            [
                TYPE => TYPE_DRINK,
                CUISINE => 'it',
                NAME => 'itDrink',
                PRICE => 10
            ]
        ];
        $italianDesserts = [
            [
                TYPE => TYPE_DESSERT,
                CUISINE => 'it',
                NAME => 'itDessert',
                PRICE => 10
            ]
        ];
        $italianMainCourses = [
            [
                TYPE => TYPE_MAIN_COURSE,
                CUISINE => 'it',
                NAME => 'itMainCourse',
                PRICE => 10
            ]
        ];

        $cuisinePl = new Cuisine('pl', 'Polish');
        $cuisineIt = new Cuisine('it', 'Italian');
        $cuisines = [$cuisinePl,$cuisineIt];

        $expectedPolishDrinks = [
            new Drink('plDrink', 10, $cuisinePl)
        ];
        $expectedPolishDesserts = [
            new Dessert('plDessert', 10, $cuisinePl)
        ];
        $expectedPolishMainCourses = [
            new MainCourse('plMainCourse', 10, $cuisinePl)
        ];
        $expectedItalianDrinks = [
            new Drink('itDrink', 10, $cuisineIt)
        ];
        $expectedItalianDesserts = [
            new Dessert('itDessert', 10, $cuisineIt)
        ];
        $expectedItalianMainCourses = [
            new MainCourse('itMainCourse', 10, $cuisineIt)
        ];
        $offer = array_merge($polishDrinks, $polishDesserts, $polishMainCourses, $italianDrinks, $italianDesserts, $italianMainCourses);

        $repository = new Repository($offer, $cuisines);

        $this->assertEquals($expectedPolishDrinks, $repository->listDrinksByCuisine($cuisinePl));
        $this->assertEquals($expectedPolishDesserts, $repository->listDessertsByCuisine('pl'));
        $this->assertEquals($expectedPolishMainCourses, $repository->listMainCoursesByCuisine('pl'));
        $this->assertEquals($expectedItalianDrinks, $repository->listDrinksByCuisine('it'));
        $this->assertEquals($expectedItalianDesserts, $repository->listDessertsByCuisine('it'));
        $this->assertEquals($expectedItalianMainCourses, $repository->listMainCoursesByCuisine('it'));

        $this->assertEquals($expectedItalianDrinks[0], $repository->getDrinkById('itDrink'));
        $this->assertEquals($expectedItalianDesserts[0], $repository->getDessertById('itDessert'));
        $this->assertEquals($expectedItalianMainCourses[0], $repository->getMainCourseById('itMainCourse'));
    }
}