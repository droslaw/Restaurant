<?php

use Restaurant\Meal\MainCourse;

class CourseTest extends PHPUnit_Framework_TestCase
{
    public function testGetName()
    {
        $expectedName = 'course-name';
        $course = new MainCourse($expectedName, 0);
        $this->assertEquals($expectedName, $course->getName());
    }

    public function testGetPrice()
    {
        $expectedPrice = 100;
        $course = new MainCourse('dummy-name', $expectedPrice);
        $this->assertEquals($expectedPrice, $course->getPrice());
    }
}