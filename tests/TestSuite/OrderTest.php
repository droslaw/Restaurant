<?php

use Restaurant\Meal\MainCourse;
use Restaurant\Meal\Dessert;
use Restaurant\Meal\Lunch;
use Restaurant\Meal\Drink;
use Restaurant\Order\Order;

class OrderTest extends PHPUnit_Framework_TestCase
{
    public function testPriceIsSumOfItemsPrice()
    {
        $order = new Order();
        $lunch = new Lunch(
            new MainCourse('main-course', 10),
            new Dessert('dessert', 10)
        );

        $order->addItem($lunch);
        $order->addItem(new Drink('drink', 5));

        $this->assertEquals(25, $order->getPrice());
    }
}