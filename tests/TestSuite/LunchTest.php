<?php

use Restaurant\Meal\Lunch;
use Restaurant\Meal\Dessert;
use Restaurant\Meal\MainCourse;

class LunchTest extends PHPUnit_Framework_TestCase
{
    public function testLunchConsistOfDessertOrMainCourseOrBoth()
    {
        $dessert = new Dessert('dessert', 0);
        $mainCourse = new MainCourse('main-course', 0);
        $lunch = new Lunch($mainCourse, $dessert);

        $this->assertSame($dessert, $lunch->getDessert());
        $this->assertSame($mainCourse, $lunch->getMainCourse());
    }
    
    public function testLuchPriceIsSumOfMainCoursePriceAndDessertPrice()
    {
        $lunch = new Lunch(
            new MainCourse('main', 10),
            new Dessert('dessert', 10)
        );

        $this->assertEquals(20, $lunch->getPrice());
    }
}
