<?php

error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', true);

use Restaurant\Cuisine;
use Restaurant\View\OfferView;
use Restaurant\Storage\Repository;
use Restaurant\Order\Builder;
use Restaurant\View\OrderView;

require 'bootstrap.php';

$cuisines = [
    new Cuisine('pl', 'Polish'),
    new Cuisine('it', 'Italian'),
    new Cuisine('mx', 'Mexican')
];
$offer = require 'data/offer.php';
$repository = new Repository($offer, $cuisines);
$offerView = new OfferView($repository);
$orderView = new OrderView();

echo $offerView->render() . PHP_EOL;

$orderBuilder = new Builder($repository);
$order = $orderBuilder->startOrder()
    ->addDrink('pl-Drink1', true)
    ->addDrink('it-Drink1', false, true)
    ->addDrink('mx-Drink3', true, true)
    ->addDrink('pl-Drink2')
    ->addLunch('pl-MainCourse1', 'pl-Dessert1')
    ->addLunch('mx-MainCourse2', 'mx-Dessert3')
    ->addLunch('it-MainCourse3', 'pl-Dessert2')
    ->getOrder();

echo PHP_EOL . 'Your Order:' . PHP_EOL;

echo $orderView->render($order);
echo PHP_EOL;